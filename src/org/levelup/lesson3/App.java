package org.levelup.lesson3;

public class App {

    // <return type> method_name(<parameter type> parameter_name, ...) {}
    public static void main(String[] args) {
        // int a;

        // Teacher t; -> null
        // объект, экземпляр, инстанс, object, reference, ссылка, instance
        Teacher t = new Teacher();
        t.name = "Дмитрий";
        System.out.println(t.s.name);

        System.out.println(t.name);
        String result = t.talk(10);
        t.talk(14, "Java");
        System.out.println(result);

        Teacher mathTeacher = new Teacher();
        mathTeacher.name = "Олег";
        mathTeacher.age = 34;
        mathTeacher.experience = 10;
        System.out.println("Учитель математики: " + mathTeacher.name);

        // Teacher[] teachers = new Teacher[2];
        // teachers[0] = t;
        // teachers[1] = mathTeacher;

        // Teacher[] teachers = {
        //          new Teacher(),
        //          new Teacher()
        // };
        Teacher[] teachers = { t, mathTeacher };
        for (int i = 0; i < teachers.length; i++) {
            // System.out.println(teachers[i].name);
            teachers[i].print();
        }

        Teacher javaTeacher = new Teacher("Сергей", 24, 3);
        javaTeacher.print();
        // System.out.println(javaTeacher.name + " " + javaTeacher.age + " " + javaTeacher.experience);
    }

}
