package org.levelup.lesson3;

public class Subject {

    String name;
    int hours;

    Subject(String subjectName, int subjectHours) {
        name = subjectName;
        hours = subjectHours;
    }

}
