package org.levelup.lesson3;

import java.util.Random;

public class Teacher {

    // Поле, field
    Subject s;
    String name;
    int age;
    int experience;

    Teacher() {
        s = new Subject("ОБЖ", 20);
    }

//    Teacher(String teacherName) {
//        name = teacherName;
//    }

    // constructor
    public Teacher(String teacherName, int teacherAge, int teacherExperience) {
        name = teacherName;
        age = teacherAge;
        experience = teacherExperience;
    }

    // <return type> method_name(<parameter type> parameter_name, ...) {}
    // ""
    String talk(long time) {
        for (long i = 0; i < time; i++) {
            System.out.println(i);
        }
        // String result = "Конец урока";
        // return result;

        // String result = "Конец";
        // return result + " урока";

        // return main2();

        return "Конец урока";
    }

    // String method(String)
    // String method(int);
    // String method(int, String);
    // String method(String, int);
    // String method(String, int); - нельзя!

    /**
     * Метод, который говорит
     * @param time время
     * @param theme тему, которую рассказывают
     * @return конец урока
     */
    String talk(long time, String theme) {
        System.out.println("Рассказывает " + theme);
        for (long i = 0; i < time; i++) {
            System.out.println(i);
        }
        return "Конец урока";
    }

    int putMark() {
        Random random = new Random();
        return random.nextInt(5) + 1;
    }

    void makeInfraction() {
        System.out.println("Замечание!");
    }

    void print() {
        System.out.println(name + " " + age + " " + experience);
    }

}
