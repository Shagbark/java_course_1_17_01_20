package org.levelup.lesson3;

@SuppressWarnings("ALL")
public class PrimitiveReferenceDifference {

    public static void main(String[] args) {
        int intVar = 10;
        Teacher teacher = new Teacher("Евгений", 0, 0);
        intVar = changeInt(intVar);
        changeString(teacher);
        changeAge(teacher);
        System.out.println(intVar);
        System.out.println(teacher.name);
        System.out.println(teacher.age);
    }
    static int changeInt(int intVar) {
        intVar = 100;
        return intVar;
    }
    static void changeString(Teacher teacher) {
        teacher.name = "Дмитрий";
    }
    static void changeAge(Teacher teacher) {
        teacher.age = 25;
    }

}
