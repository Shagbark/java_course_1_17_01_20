package org.levelup.lesson7;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListExamples {

    public static void main(String[] args) {
        // ArrayList - список на основе массива (динамический массив)
        // LinkedList - двунаправленный связный список
        List<String> strings = new LinkedList<>();
        strings.add("s1");
        strings.add("s2");
        strings.add("s5");
        strings.add("s4");
        strings.add("s2");

        System.out.println("Size: " + strings.size());
        System.out.println("4 element: " + strings.get(3));

        strings.set(4, "s8"); // изменение значения по индексу
        System.out.println("5 element: " + strings.get(4));

        System.out.println("Contains s5: " + strings.contains("s5"));
    }

}
