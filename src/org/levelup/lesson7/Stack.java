package org.levelup.lesson7;

public interface Stack<TYPE> {

    void push(TYPE obj);

    TYPE pop();

}
