package org.levelup.lesson7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapExamples {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        map.put("pagesCount", 3);
        map.put("wordsCount", 564);
        map.put("spaceCount", 190);

        System.out.println(map.get("spaceCount"));

        // Map<String, List<String>> teachers;
        // Map<String, Map<String, List<String>>> maps;
    }

}
