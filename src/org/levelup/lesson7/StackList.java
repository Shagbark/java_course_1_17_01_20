package org.levelup.lesson7;

// type erasure - стирание типов
// Outer class - внешний класс для Element
public class StackList<TYPE> implements Stack<TYPE> {

    private Element head;

    // Inner class (внутренний класс)
    private class Element {
        Element next;
        TYPE value;
        Element(TYPE value) {
            this.value = value;
        }
    }

    @Override
    public void push(TYPE obj) {
        Element element = new Element(obj);
        if (head == null) {
            head = element;
        } else {
            element.next = head;
            head = element;
        }
    }

    @Override
    public TYPE pop() {
        if (head == null) {
            return null;
        }
        TYPE value = head.value;
        head = head.next;
        return value;
    }

}