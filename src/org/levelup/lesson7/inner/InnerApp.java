package org.levelup.lesson7.inner;

import java.util.Random;

public class InnerApp {

    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();

        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        innerClass.methodInInnerClass();

        OuterClass.InnerClass ic2 = outerClass.new InnerClass();

        OuterClass oc = new OuterClass();
        OuterClass.InnerClass in = oc.new InnerClass();

        OuterClass.InnerClass ic3 = new OuterClass().new InnerClass();

        int randomNumber = new Random().nextInt();


    }

}
