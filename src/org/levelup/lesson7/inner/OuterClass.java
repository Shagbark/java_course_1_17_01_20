package org.levelup.lesson7.inner;

public class OuterClass {

    InnerClass innerClass;

    public OuterClass() {
        this.innerClass = new InnerClass();
    }

    private void methodInOuterClass() {
        innerClass.privateMethodInInnerClass();
        System.out.println("Private method in outer class ");
    }

    public class InnerClass {

        private void privateMethodInInnerClass() {

        }

        public void methodInInnerClass() {
            methodInOuterClass();
        }

    }

}
