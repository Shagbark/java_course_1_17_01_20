package org.levelup.lesson10;

public class CarApp {

    public static void main(String[] args) {
        Car car = new Car();
        car.changeColor(Color.BLACK);
        car.changeColor(Color.WHITE);

        System.out.println(Color.GREEN.getHexView());

    }

}
