package org.levelup.lesson10;

public class ClassWithStatics {

    int usualVar;
    static int staticVar;

    public static void staticMethod() {
        // usualVar = 10;
        // ClassWithStatics obj = new ClassWithStatics();
        // obj.usualVar = 34;
        staticVar = 11;
        System.out.println("Run without object");
    }

    public static class NestedClass {

    }

}
