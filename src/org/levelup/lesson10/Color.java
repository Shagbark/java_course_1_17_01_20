package org.levelup.lesson10;

// Enum

// 1. Может ли enum наследовать другой класс? Нет
// 2. Может ли enum наследовать другой enum? Нет
// 3. Может ли enum наследовать абстрактный класс? Нет
// 4. Может ли enum реализовывать интерфейс? Да
// 5. Может ли enum иметь абстрактные методы? Да
public enum Color implements Runnable {

    BLACK("000000") {
        @Override
        void doSmth() {

        }

        @Override
        public void run() {

        }
    },
    WHITE("ffffff") {
        @Override
        void doSmth() {

        }
        @Override
        public void run() {

        }
    },
    GREEN("00ff00"){
        @Override
        void doSmth() {

        }
        @Override
        public void run() {

        }
    };

    // RGB - red, green, blue
    // 255 255 255
    // ffffff
    String hexView;

    Color(String hexView) {
        System.out.println("Create enum object");
        this.hexView = hexView;
    }

    public String getHexView() {
        return hexView;
    }

    abstract void doSmth();

//    @Override
//    public void run() {
//
//    }

}
