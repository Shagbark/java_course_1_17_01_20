package org.levelup.lesson10;

public class Car {

    private int colorNumber;
    private Color color;

    public void changeColor(int colorNumber) {
        this.colorNumber = colorNumber;
    }

    public void changeColor(Color color) {
        this.color = color;
    }

    public String getColorName() {
        if (colorNumber == 1) {
            return "Black";
        } else if (colorNumber == 2) {
            return "White";
        }
        return null;
    }

}
