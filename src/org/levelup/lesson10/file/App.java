package org.levelup.lesson10.file;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("ALL")
public class App {

    public static void main(String[] args) throws IOException {
        File userFile = new File("src/users.txt");
        System.out.println(userFile == null);
        System.out.println("File exists: " + userFile.exists());

        boolean result = userFile.createNewFile();
        System.out.println("File created: " + result);

        File srcDir = new File("src");
        System.out.println("Src exists: " + srcDir.exists());

        System.out.println("Is directory: " + srcDir.isDirectory());
        System.out.println("Is file: " + userFile.isFile());

        File newDir = new File("test");
        boolean createDirResult = newDir.mkdir();
        System.out.println("Directory created: " + createDirResult);

    }

}
