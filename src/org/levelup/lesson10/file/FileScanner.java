package org.levelup.lesson10.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class FileScanner {

    public Collection<User> loadUsers(String filename) {
        File file = new File(filename);
        BufferedReader reader = null;

        Collection<User> loadedUsers = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;

            // EOF
            while ( (line = reader.readLine()) != null ) {
                // "    privet ?    " -> "privet ?"
                // "      " -> "" -> isEmpty true
                // "    " -> isEmpty -> false
                line = line.trim();
                if (!line.isEmpty()) {
                    loadedUsers.add(parseLine(line));
                }
            }

        } catch (IOException exc) {
            System.out.println(exc.getMessage());
            throw new RuntimeException(exc);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return loadedUsers;
    }

    public Collection<User> loadUsersBetterWay(String filename) {
        // try-with-resources
        Collection<User> loadedUsers = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filename)))) {
            String line;
            while ( (line = reader.readLine()) != null ) {
                line = line.trim();
                if (!line.isEmpty()) {
                    loadedUsers.add(parseLine(line));
                }
            }
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
        return loadedUsers;
    }

    private User parseLine(String line) {
        String[] elements = line.split(";");
        return new User(
            elements[0],
            elements[1],
            Integer.parseInt(elements[2]),
            elements[3]
        );
    }

}
