package org.levelup.lesson10.file;

import java.io.File;
import java.util.Collection;

public class UserApp {

    public static void main(String[] args) {
        FileScanner scanner = new FileScanner();

        Collection<User> users = scanner.loadUsers("users.txt");
        System.out.println(users);
    }

}
