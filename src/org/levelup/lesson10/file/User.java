package org.levelup.lesson10.file;

public class User {

    private String login;
    private String password;
    private int age;
    private String name;

    public User(String login, String password, int age, String name) {
        this.login = login;
        this.password = password;
        this.age = age;
        this.name = name;
    }
}
