package org.levelup.lesson10;

public class App {

    public static void main(String[] args) {
        // <Class name>.method() .field
        ClassWithStatics.staticMethod();
        ClassWithStatics.staticVar = 10;

        ClassWithStatics.NestedClass nestedClass =
                new ClassWithStatics.NestedClass();

    }

}
