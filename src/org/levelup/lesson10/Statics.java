package org.levelup.lesson10;

@SuppressWarnings("ALL")
public class Statics {

    public static void main(String[] args) {
        Statics obj = null;
        obj.hello(); // -> Statics.hello();
    }

    static void hello() {
        System.out.println("Hello");
    }

}
