package org.levelup.lesson4;

import org.levelup.lesson3.Teacher;

@SuppressWarnings("ALL")
public class Integers {

    public static void main(String[] args) {
        Teacher teacher1 = new Teacher("", 0, 0);
        Teacher teacher2 = new Teacher("", 0, 0);
        System.out.println(teacher1 == teacher2); // false

        Integer i1 = new Integer(126);
        Integer i2 = new Integer(126);
        Integer i3 = new Integer(129);
        Integer i4 = new Integer(129);

        // false, false
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
        System.out.println();

        Integer i5 = 126; // Integer i5 = Integer.valueOf(126)
        Integer i6 = 126;
        Integer i7 = 129;
        Integer i8 = 129;

        // true, true
        // true, false
        // false, true
        // false, false
        System.out.println(i5 == i6);
        System.out.println(i7 == i8);
    }

}
