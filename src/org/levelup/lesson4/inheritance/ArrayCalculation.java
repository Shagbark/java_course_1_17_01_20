package org.levelup.lesson4.inheritance;

public class ArrayCalculation {

    public static void main(String[] args) {
        Shape rectangle = new Rectangle(10, 10);

        Shape[] shapes = new Shape[6];
        shapes[0] = new Rectangle(10, 30);
        shapes[1] = new Shape(new int[] { 3, 5, 6, 6, 7});
        shapes[2] = new Square(20);
        shapes[3] = new Square(3);
        shapes[4] = new Square(5);
        shapes[5] = new Rectangle(30, 4);

        double totalSquare = 0.d;
        for (int i = 0; i < shapes.length; i++) {
            totalSquare += shapes[i].calculateSquare();
        }

        System.out.println(totalSquare);
    }



}
