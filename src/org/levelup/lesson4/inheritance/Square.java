package org.levelup.lesson4.inheritance;

public class Square extends Rectangle {

    Square(int side) {
        super(side, side);
        System.out.println("Square constructor");
    }

    @Override
    double calculateSquare() {
        return sides[0] * sides[0];
    }

}
