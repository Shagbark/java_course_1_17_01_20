package org.levelup.lesson4.inheritance;

// Shape - superclass, base class, родитель, предок
// Rectangle - subclass, подкласс, наследник, ребенок
public class Rectangle extends Shape {

    String color;

    Rectangle(int width, int height) {
        super(new int[] { width, height, width, height });
        System.out.println("Rectangle construct");
    }

    // void method() {}
    // int max() { return max; }

    void changeColor(String color) {
        if (color != null) {
            this.color = color;
            super.color = color;
        }
    }

    @Override
    double calculateSquare() {
        // code here
        // super.calculateSquare();
        // code here
        return sides[0] * sides[1];
    }

}
