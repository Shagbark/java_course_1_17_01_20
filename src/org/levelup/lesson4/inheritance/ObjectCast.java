package org.levelup.lesson4.inheritance;

@SuppressWarnings("ALL")
public class ObjectCast {

    public static void main(String[] args) {
        Square square = new Square(10);

        Rectangle rectangleFromSquare = square;
        Shape shapeFromRectangle = rectangleFromSquare;

        Object objectFromSquare = square;

        Rectangle rectangleFromObject = (Rectangle) objectFromSquare;
        Square castSquareFromRectangle = (Square) rectangleFromSquare;

        Rectangle rectangle = new Rectangle(10, 10);
        Square squareFromRectangle = (Square) rectangle;

    }

}
