package org.levelup.lesson4.inheritance;

public class App {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(45, 8);

        double perimeter = rectangle.calculatePerimeter();
        System.out.println("Rectangle perimeter: " + perimeter);

        double square = rectangle.calculateSquare();
        System.out.println("Rectangle square: " + square);

        Square squareShape = new Square(30);
        System.out.println("Square: " + squareShape.calculateSquare());

    }

}
