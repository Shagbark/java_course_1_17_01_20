package org.levelup.lesson4.inheritance;

@SuppressWarnings("ALL")
public class Shape {

    String color;
    int[] sides;

    Shape(int[] sides) {
        System.out.println("Shape constructor");
        this.sides = sides;
    }

    double calculatePerimeter() {
        double perimeter = 0;

        for (int i = 0; i < sides.length; i++) {
            perimeter += sides[i];
        }

        return perimeter;
    }

    double calculateSquare() {
        System.out.println("Мы не умеем вычислять площадь фигуры");
        return 0;
    }

}
