package org.levelup.lesson4;

public class ArrayUtil {

    // int[] array = new int[0];
    // max(array); -> ArrayIndexOutOfBoundsException

    // max(null);
    int method(int a) {
        return a;
    }

    Integer max(int[] array) {
        // NullPointerException
        if (array == null || array.length == 0) {
            System.out.println("Array is empty...");
            return null;
        }

        int maxElement = array[0];
        // ... code
        //
        return 0;
    }

}
