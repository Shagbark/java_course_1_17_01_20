package org.levelup.lesson9;

public class User implements Comparable<User> {

    private String login;
    private String password;

    private String name;
    private String lastName;
    private int age;

    public User(String login, String password, String name, String lastName, int age) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // user1 - this
    // user2 - o
    // user1.compareTo(user2)
    // < 0 -> user1 < user2
    // = 0 -> user1 = user2
    // > 0 -> user1 > user2
    @Override
    public int compareTo(User o) {
        // ComparisonViolationException
        // a > b, b > c -> a > c
        // a > b, b < a
        // a = a

        if (login == o.login) {
            return 0;
        }
        if (login == null) {
            return -1;
        }
        if (o.login == null) {
            return 1;
        }

        return login.compareTo(o.login);
    }
}
