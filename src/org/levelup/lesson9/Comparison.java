package org.levelup.lesson9;

import java.util.*;

public class Comparison {

    public static void main(String[] args) {
        List<User> users = new ArrayList<>();
        users.add(new User("admin", "admin", "Admin", "Adminov", 14));
        users.add(new User("user", "qwerty", "Dmitry", "Ivanov", 34));
        users.add(new User("petr", "petr", "Petr", "Petrov", 25));
        users.add(new User("sid", "password", "Ivan", "Sidorov", 54));

        for (User user : users) {
            System.out.println(user.getLogin());
        }
        System.out.println();

        Collections.sort(users, new UserAgeComparator());

        for (User user : users) {
            System.out.println(user.getLogin());
        }

        System.out.println();

        // Comparator - name
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });

        for (User user : users) {
            System.out.println(user.getName());
        }

        // ()
        // o1
        // (o1, o2)
        // (o1, o2, ... )

        Collections.sort(users, (o1, o2) -> {
            // code here ...
            return o1.getLastName().compareToIgnoreCase(o2.getName());
        });
        Collections.sort(users, (o1, o2) -> o1.getLastName().compareToIgnoreCase(o2.getName()));

    }

}
