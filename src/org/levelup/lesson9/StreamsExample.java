package org.levelup.lesson9;

import com.sun.source.tree.UsesTree;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class StreamsExample {

    public static void main(String[] args) {
        Collection<String> strings = new ArrayList<>();
        strings.add("rotew");
        strings.add("rosdfgsdfsbdtew");
        strings.add("ftrhthgr");
        strings.add("sfdgsd");
        strings.add("rosdfgsdftew");
        strings.add("ewt3");
        strings.add("g3erger");
        strings.add("wergwerrg");
        strings.add("wegwerg");
        strings.add("htrwwrgwertwergweff");

//        for (String string : strings) {
//            System.out.println(string);
//        }

//        strings.forEach(new Consumer<String>() {
//            @Override
//            public void accept(String s) {
//                System.out.println(s);
//            }
//        });

        // strings.forEach(s -> System.out.println(s));
        strings.forEach(System.out::println);
        System.out.println();

        List<String> filteredStrings = strings.stream()
                // boolean test(String s);
                .filter(s -> s.length() > 7)
                .collect(Collectors.toList());
        filteredStrings.forEach(System.out::println);


        // Consumer<T> - void accept(T t);
        // Predicate<T> - boolean test(T t);
        // Supplier<T> - T get();
        // Function<T, R> - R apply(T t);
        // BiConsumer<T, R> - void accept(T t, R r);
        // BiFunction<T, K, R> - R apply(T t, K k)

        System.out.println();
        strings.stream()
//                .map(s -> {
//                    return s + "qwerty";
//                })
                .map(s -> s + "qwerty")
                .peek(System.out::println);
                // .forEach(System.out::println);

        Collection<Integer> salaries = new ArrayList<>();
        salaries.add(20000);
        salaries.add(60640);
        salaries.add(87000);
        salaries.add(90000);
        salaries.add(54000);

        boolean result = salaries.stream()
                .filter(salary -> salary > 30000)
                // el1 && el2 && el3 > 31000
                // anyMatch -> el1 || el2 || el3 > 31000
                .allMatch(salary -> salary > 31000);
        System.out.println(result);

        List<Integer> updatedSalaries = salaries.stream()
                .map(salary -> salary + 10000)
                .collect(Collectors.toList());

        // Stream<T>
        // IntStream
        // DoubleStream
        // LongStream
        double sumSalary = salaries.stream()
                .mapToDouble(salary -> {
                    return salary;
                })
                .sum();

        int sumOfSymbols = strings.stream()
                .mapToInt(s -> s.length())
                .sum();

        Map<String, Integer> mapSalaries = new HashMap<>();
        mapSalaries.put("Ivanov", 46433);
        mapSalaries.put("Petrov", 65123);
        mapSalaries.put("Sidorov", 74545);
        mapSalaries.put("Alekseev", 85242);

        mapSalaries.forEach((lastName, salary) -> System.out.println(lastName + " " + salary));

        Set<Map.Entry<String, Integer>> entries = mapSalaries.entrySet();

        System.out.println();
        Map.Entry<String, Integer> maxSalary = entries.stream()
                .max((e1, e2) -> Integer.compare(e1.getValue(), e2.getValue()))
                .get();
        System.out.println(maxSalary.getKey() + " " + maxSalary.getValue());
//        List<String> list = new ArrayList<>();
//        for (String s : strings) {
//            if (s.length() > 7) {
//                list.add(s);
//            }
//        }
//        for (String s : list) {
//            System.out.println(s);
//        }

    }

}
