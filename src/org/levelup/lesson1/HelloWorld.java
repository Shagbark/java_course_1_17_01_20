package org.levelup.lesson1;

public class HelloWorld {

    public static void main(String[] args) {
        // Line comments here
        /*
            Comments
         */
        System.out.println("Hello world!");

        int a;
        a = 10;

        int b = 17;

        int c;
        c = a * b + a;
        System.out.println(c);
        System.out.println("Результат операции: " + c);

        System.out.println(a + b + " result");
        System.out.println("result: " + a + b);

    }

}
