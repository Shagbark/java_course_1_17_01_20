package org.levelup.lesson2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Loop {

    public static void main(String[] args) {
        //
        int even = 0;
        for (int i = 0; i < 0x64; i++) {
            System.out.println(even);
            even += 2;
        }

        // Инициализация - int i = 0;
        // i(0) < 10 -> true, sout(i); i = 0 + 1 = 1
        // i(1) < 10 -> true, sout(i); i = 1 + 1 = 2
        // i(2) < 10 -> true, sout(i); i = 2 + 1 = 3
        // ...
        // i(10) < 10 -> false

        // for (;;) { }
        // while (true) { }

        // ++ - increment
        // -- - decrement
        int var = 10;
//        var++; var += 1; var = var + 1;
//        var--;
        // prefix increment - ++var;
        // postfix increment - var++;

        // 12, 10
        // 10, 11
        // 11, 10
        // 10, 12
        System.out.println(var++);
        // sout(var);
        // var = var + 1;
        // var = var + 1;
        // sout(var);
        System.out.println(++var);

        int counter = 0;
        do {
            System.out.println(counter++);
        } while (counter < 5);


        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        while (number != 0) {
            System.out.println("Введите 0");
            number = sc.nextInt();
        }

//        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
//            String line = reader.readLine();
//            int number2 = Integer.parseInt(line);
//        } catch (IOException exc) {
//            throw new RuntimeException(exc);
//        }

    }

}
