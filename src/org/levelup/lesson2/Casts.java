package org.levelup.lesson2;

public class Casts {

    // psvm + Tab
    public static void main(String[] args) {

        int intVar = 10;
        long longVar = 542;
        float floatVar = 64.233f;

        byte byteVar = (byte) intVar;
        double doubleVar = floatVar;

        byte a = 100;
        byte b = 100;
        // byte c = a + b;

        int fromDouble = (int) 45.68d;
        // sout + Tab
        System.out.println(fromDouble);

        b += 20; // -> b = (byte)(b + 20);

        byte bt = (byte) 1130;
        System.out.println(bt);

    }

}
