package org.levelup.lesson2;

import java.util.Random;

public class Array {

    public static void main(String[] args) {
        int[] prices = new int[6];
        prices[0] = 55;
        prices[1] = 34;
        prices[3] = 12;
        prices[5] = 12;

        System.out.println(prices[2]);
        System.out.println("Длина массива: " + prices.length);

        Random random = new Random();
        for (int i = 0; i < prices.length; i++) {
            prices[i] = random.nextInt(5);
            System.out.println(prices[i]);
        }

    }

}
