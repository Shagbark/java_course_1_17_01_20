package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        Random randomizer = new Random();
        // nextInt - [0, 32)
        // nextInt(5) - [0, 5)
        // [-5, 5] -> nextInt(11) - 5; -> [0, 11) - 5 -> [-5, 6)
        // [-3, 3] -> [-3, 4) -> nextInt(7) - 3
        int secretNumber = randomizer.nextInt(3);

        // sout + Tab
        System.out.println("Введите число:");
        // Scanner - чтение с консоли
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();

//        if (number == secretNumber) {
//            System.out.println("Вы угадали!");
//            // some code here ...
//        } else {
//            System.out.println("Вы не угадали! Число = " + secretNumber);
//        }
//        if (secretNumber == number) {
//
//        } else {
//            if (secretNumber > number) {
//
//            } else {
//
//            }
//        }

        if (secretNumber > number) {
            System.out.println("Вы ввели число меньше, чем загаданное.");
        } else if (secretNumber < number) {
            System.out.println("Вы ввели число больше, чем загаданное.");
        } else {
            System.out.println("Вы угадали");
        }

//        if (secretNumber > number) {
//
//        }
//        if (secretNumber < number) {
//
//        }
//        if (secretNumber == number) {
//
//        }

    }

}
