package org.levelup.lesson6;

public abstract class Image {

    // [1, 2, 4, 5, 6]

    // [
    //  [1, 2, 4, 5, 6]
    //  [2, 6, 7, 5, 4]
    // ]
    private int[][] bytes;

    public Image(int[][] bytes) {
        this.bytes = bytes;
    }

    public Image() {}

    public abstract void render();

    public void setBytes(int[][] bytes) {
        this.bytes = bytes;
    }

}
