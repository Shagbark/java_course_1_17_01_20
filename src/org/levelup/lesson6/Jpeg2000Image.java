package org.levelup.lesson6;

public class Jpeg2000Image extends JpegImage {

    @Override
    public void compress() {
        System.out.println("JPEG2000 compression");
    }

    @Override
    public void render() {
        compress();
        System.out.println("Render compressed JPEG image");
    }

}
