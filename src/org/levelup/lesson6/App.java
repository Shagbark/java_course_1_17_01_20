package org.levelup.lesson6;

public class App {

    public static void main(String[] args) {
        Image image = new Jpeg2000Image();

        Scalable scalableImage = new BmpImage();
        scalableImage.scale();
    }

}
