package org.levelup.lesson6;

public abstract class JpegImage extends Image {

    public abstract void compress();

    @Override
    public void render() {

    }
}
