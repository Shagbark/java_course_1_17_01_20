package org.levelup.lesson6.structure;

public class OneWayList extends AbstractStructure {

    private ListElement head;

    @Override
    public void add(int value) {
        ListElement element = new ListElement(value);
        if (head == null) {
            head = element;
        } else {
            ListElement current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = element;
        }
        numberOfElements++;
    }

    @Override
    public void removeElement(int value) {

    }

}
