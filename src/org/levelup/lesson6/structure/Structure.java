package org.levelup.lesson6.structure;

public interface Structure {

    void add(int value);

    // Удаляем, все элементы, которые имеют заданное значение
    void removeElement(int value);

    // Количество данных в структуре
    int getSize();

    boolean isEmpty();

}
