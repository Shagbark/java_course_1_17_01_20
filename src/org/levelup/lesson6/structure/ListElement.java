package org.levelup.lesson6.structure;

public class ListElement {

    ListElement next;
    int value;

    ListElement(int value) {
        this.value = value;
    }

}
