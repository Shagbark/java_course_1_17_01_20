package org.levelup.lesson6.structure;

public class StructureApp {

    public static void main(String[] args) {
        Structure s = new OneWayList();

        s.add(10);
        s.add(15);
        s.add(16);
        s.add(65);
        s.add(76);
        System.out.println("Количество элементов: " + s.getSize());
    }

}
