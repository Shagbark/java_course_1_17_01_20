package org.levelup.lesson6.structure;

// Список на основе массива (динамический массив)
public class DynamicArray extends AbstractStructure {

    private int[] elements;

    public DynamicArray(int initialCapacity) {
        this.elements = new int[initialCapacity];
    }

    @Override
    public void add(int value) {
        if (elements.length == numberOfElements) {
            // Увеличиваем размер массива elements
            int[] oldArray = elements;
            elements = new int[(int)(elements.length * 1.5)];
            System.arraycopy(oldArray, 0, elements, 0, oldArray.length);
        }

        // v = 10
        // n = 0
        // [0, 0, 0, 0]
        // [10, 0, 0, 0], n = 1

        // v = 5
        // n = 1
        // [10, 0, 0, 0]
        // [10, 5, 0, 0], n = 2
        elements[numberOfElements++] = value;
        // numberOfElements++;
    }

    @Override
    public void removeElement(int value) {

    }

}
