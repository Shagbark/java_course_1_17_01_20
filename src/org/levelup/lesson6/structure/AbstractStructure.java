package org.levelup.lesson6.structure;

public abstract class AbstractStructure implements Structure {

    protected int numberOfElements;

    @Override
    public int getSize() {
        return numberOfElements;
    }

    @Override
    public boolean isEmpty() {
        return numberOfElements == 0;
    }
}
