package org.levelup.lesson6;

import java.io.Serializable;

public class BmpImage extends Image implements Scalable, Serializable {

    public BmpImage() {
        super(new int[0][0]);
    }

    @Override
    public void render() {
        System.out.println("Render BMP");
    }

    @Override
    public void scale() {

    }
}
