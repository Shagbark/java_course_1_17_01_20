package org.levelup.lesson5.comparison;

@SuppressWarnings("ALL")
public class App {

    public static void main(String[] args) {
        Point first = new Point(12, 2, "A");
        Point second = new Point(1 ,2, "A");

        boolean refComparison = first == second; // false
        boolean comparison = first.equals(second);
        System.out.println(comparison);
        System.out.println(first.hashCode());
        System.out.println(second.hashCode());

    }

}
