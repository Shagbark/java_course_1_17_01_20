package org.levelup.lesson5.comparison;

import java.util.Objects;

@SuppressWarnings("ALL")
public class Point {

    private int x;
    private int y;
    private String name;

    public Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    // this - first
    // obj - second
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        // if (!(obj instanceof Point)) return false;
        if (obj == null || getClass() != obj.getClass()) return false;

        Point other = (Point) obj;
        return x == other.x && y == other.y &&
                Objects.equals(name, other.name);
                // (name != null && name.equals(other.name));
    }

    @Override
    public int hashCode() {
//        int result = 17;
//
//        result += 31 * result + x;
//        result += 31 * result + y;
//        result += 31 * result + name.hashCode();
//
//        return result;`
        return Objects.hash(x, y, name);
    }

}
