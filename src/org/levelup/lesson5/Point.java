package org.levelup.lesson5;

// public
// protected
// default-package (private-package)
// private

public class Point {

    protected Point p;
    private int x;
    private int y;

    protected double calculateDistance(Point point) {
        // code ...
        // this.x, this.y - первая точка
        // point.x, point.y - вторая точка
        return calculateDistance(this, point);
    }

    private double calculateDistance(Point first, Point second) {
        return 0;
    }

    // getters/setters
    // getter
    // public <TYPE> get<FIELD_NAME>() { return FIELD_NAME; }
    public int getX() {
        return x;
    }
    // setter
    // public void set<FIELD_NAME>(<TYPE> fieldName) { this.fieldName = fieldName; }
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point getP() {
        return p;
    }

    public void changePoint(Point point) {
        if (point == null) {
            // code ...
        }
    }

}
